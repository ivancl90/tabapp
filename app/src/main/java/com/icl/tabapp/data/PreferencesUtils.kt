package com.icl.tabapp.data

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.icl.tabapp.Constants.Constants
import com.icl.tabapp.extensions.logBuilder

class PreferencesUtils(context: Context) : AppCompatActivity() {

    val context: Context = context


    lateinit var preferences: SharedPreferences

    private fun initPreferences() {
        preferences = PreferenceManager.getDefaultSharedPreferences(context)
    }

    fun saveSetPreferences(nameList: ArrayList<String>, namePreference: String) {
        Log.d(Constants.APP_NAME, logBuilder("saveSetPreferences", Constants.STRING_EMPTY))
        initPreferences()
        val editor = preferences.edit()
        var preferenceElement = getPreference(namePreference) ?: mutableSetOf()
        nameList.forEach { it: String ->
            preferenceElement.add(it)
        }

        editor.clear()
        editor.putStringSet(namePreference, preferenceElement)
        editor.apply()
    }

    fun savePreferences(name: String, namePreference: String) {
        Log.d(Constants.APP_NAME, logBuilder("savePreferences", Constants.STRING_EMPTY))
        initPreferences()
        val editor = preferences.edit()
        var preferenceElement = getPreference(namePreference) ?: mutableSetOf()

        preferenceElement.add(name)
        editor.clear()
        editor.putStringSet(namePreference, preferenceElement)
        editor.apply()
    }

    fun getPreference(namePreference: String): MutableSet<String>? {
        Log.d(Constants.APP_NAME, logBuilder("getPreference", Constants.STRING_EMPTY))
        initPreferences()
        val prfSetElement = preferences.getStringSet(namePreference, null)
        prfSetElement?.forEach { Log.d(Constants.APP_NAME, it) }
        return prfSetElement
    }

}