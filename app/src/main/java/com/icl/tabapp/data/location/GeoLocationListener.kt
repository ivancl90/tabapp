package com.icl.tabapp.data.location

import android.location.Location
import android.location.LocationListener
import android.os.Bundle
import android.util.Log

class GeoLocationListener {

    private val locationListener: LocationListener = object : LocationListener {
        override fun onLocationChanged(location: Location) {
            Log.d("LOCATION CHANGED",location.longitude.toString() + ":" + location.latitude.toString())
        }

        override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}
        override fun onProviderEnabled(provider: String) {}
        override fun onProviderDisabled(provider: String) {}
    }
}