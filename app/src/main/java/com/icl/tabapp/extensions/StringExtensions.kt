package com.icl.tabapp.extensions

import com.icl.tabapp.BuildConfig
import android.util.Log

fun String.log(tag:String = "TABAPP-CICE"){
    if (BuildConfig.DEBUG)
        Log.d(tag, this)
}

fun logBuilder(method: String, extra: String): String {
    return String.format("Method: %1\$s , desc: %2\$s", method, extra)
}