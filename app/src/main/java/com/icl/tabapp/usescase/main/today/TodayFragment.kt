package com.icl.tabapp.usescase.main.today

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.icl.tabapp.R
import com.icl.tabapp.databinding.FragmentHomeBinding
import com.icl.tabapp.usescase.main.MainActivity
import com.icl.tabapp.usescase.main.MainGlobalFragment

class TodayFragment : MainGlobalFragment() {

    private lateinit var binding: FragmentHomeBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_today, container, false)
        initViews()
        return binding.root
    }


    private fun initViews() {
        binding.editName.addTextChangedListener(object: TextWatcher{
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                (activity as MainActivity).mainViewModel.name = s.toString()
            }
        })
    }
}