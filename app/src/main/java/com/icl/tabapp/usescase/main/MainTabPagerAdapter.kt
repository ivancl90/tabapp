package com.icl.tabapp.usescase.main

import android.content.res.TypedArray
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

class MainTabPagerAdapter(private val items: TypedArray, fm: FragmentManager): FragmentStatePagerAdapter(fm){


    override fun getItem(position: Int): MainGlobalFragment {
        return MainGlobalFragment.instantiate(items.getResourceId(position,0))
    }

    override fun getCount(): Int {
        return items.length()
    }
}