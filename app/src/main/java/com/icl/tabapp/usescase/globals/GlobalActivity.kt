package com.icl.tabapp.usescase.globals

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.icl.tabapp.extensions.log

abstract class GlobalActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d("CICE", localClassName)
        localClassName.toString().log()
    }
}