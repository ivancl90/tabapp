package com.icl.tabapp.usescase.main.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.icl.tabapp.R
import com.icl.tabapp.databinding.FragmentProfileBinding
import com.icl.tabapp.usescase.main.MainActivity
import com.icl.tabapp.usescase.main.MainGlobalFragment

class ProfileFragment : MainGlobalFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var binding: FragmentProfileBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false)
        binding.name  = (activity as MainActivity).mainViewModel.name
        return binding.root
    }

}