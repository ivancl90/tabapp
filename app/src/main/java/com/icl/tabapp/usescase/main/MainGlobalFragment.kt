package com.icl.tabapp.usescase.main

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.icl.tabapp.R
import com.icl.tabapp.usescase.main.today.TodayFragment
import com.icl.tabapp.usescase.main.profile.ProfileFragment

open class MainGlobalFragment : Fragment() {

    companion object {
        fun instantiate(section: Int, args: Bundle? = null): MainGlobalFragment {
            val fragment: MainGlobalFragment

            when (section) {
                R.string.tab_title_today -> fragment = TodayFragment()
                R.string.tab_title_profile -> fragment = ProfileFragment()
                else -> fragment = MainGlobalFragment()
            }
            fragment?.arguments = args
            return fragment
        }
    }
}