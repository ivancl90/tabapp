package com.icl.tabapp.usescase.main

import android.content.res.TypedArray
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.google.android.material.tabs.TabLayout
import com.icl.tabapp.R
import com.icl.tabapp.databinding.ActivityMainBinding
import com.icl.tabapp.extensions.log

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    lateinit var mainViewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        mainViewModel = MainViewModel.create(this)
        initializeView()
    }


    private fun initializeView() {
        var tabTitles: TypedArray = resources.obtainTypedArray(R.array.tab_titles)

        binding.viewPagerTabs.adapter = MainTabPagerAdapter(tabTitles, supportFragmentManager)
        binding.tabLayout.setupWithViewPager(binding.viewPagerTabs)


        for (i in 0 until tabTitles.length()) {
            binding.tabLayout.getTabAt(i)?.text = tabTitles.getText(i)
        }

        /*Añadir listener*/
        binding.tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {
                "reselected" + tab?.text.toString().log()
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                tab?.text.toString().log()
            }
        })

    }
}
