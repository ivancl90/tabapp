package com.icl.tabapp.usescase.splash

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import com.icl.tabapp.R
import com.icl.tabapp.config.ConfigProperties
import com.icl.tabapp.usescase.globals.GlobalActivity
import com.icl.tabapp.usescase.main.MainActivity
import java.net.URL
import android.location.Location
import android.net.Uri
import android.provider.Settings
import android.widget.TextView
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.icl.tabapp.BuildConfig

class SplashActivity : GlobalActivity() {

    var locationManager: LocationManager?=null

    @SuppressLint("MissingPermission")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        Thread.sleep(3000)
        var intent: Intent
        intent = Intent(this@SplashActivity, MainActivity::class.java)

        if (!checkPermissions()) {
            requestPermissions()
        } else {
            locationManager = getSystemService(LOCATION_SERVICE) as LocationManager?
            locationManager?.getLastKnownLocation(LocationManager.GPS_PROVIDER)
        }


        getCurrentTime()
        startActivity(intent)

    }

    private fun getCurrentTime() {
        var result: String
        Thread {

            //getLastKnowLocation
            result = URL(ConfigProperties.getValue("url.weather", this.applicationContext)).readText()
            this.runOnUiThread {
                Log.d("TIEMPO RECIBIDO", result)

               /* categoryList = Gson().fromJson(result, Categories::class.java)
                Log.d(Constants.APP_NAME, categoryList!!.size.toString())
                loadCategoriesIntoPreferences()
                var intent = Intent(this@SplashActivity, CategoryActivity::class.java)
                startActivity(intent)*/
            }
        }.start()
    }
    private fun showSnackbar(mainTextStringId: Int, actionStringId: Int,
                             listener: View.OnClickListener) {

        Toast.makeText(this@SplashActivity, getString(mainTextStringId), Toast.LENGTH_LONG).show()
    }

    private fun checkPermissions(): Boolean {
        val permissionState = ActivityCompat.checkSelfPermission(this,
            Manifest.permission.ACCESS_FINE_LOCATION)
        return permissionState == PackageManager.PERMISSION_GRANTED
    }

    private fun startLocationPermissionRequest() {
        ActivityCompat.requestPermissions(this@SplashActivity,
            arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION),
            REQUEST_PERMISSIONS_REQUEST_CODE)
    }

    private fun requestPermissions() {
        val shouldProvideRationale = ActivityCompat.shouldShowRequestPermissionRationale(this,
            Manifest.permission.ACCESS_FINE_LOCATION)
            Manifest.permission.ACCESS_FINE_LOCATION
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.")

            showSnackbar(
                R.string.permission_rationale, android.R.string.ok,
                View.OnClickListener {
                    startLocationPermissionRequest()
                })

        } else {
            Log.i(TAG, "Requesting permission")
            startLocationPermissionRequest()
        }
    }


/**
 * Callback received when a permissions request has been completed.
 */

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        Log.i(TAG, "onRequestPermissionResult")
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.size <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.i(TAG, "User interaction was cancelled.")
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission granted.
                //getLastLocation()
            } else {

                showSnackbar(
                    R.string.permission_denied_explanation, R.string.settings,
                    View.OnClickListener {
                        // Build intent that displays the App settings screen.
                        val intent = Intent()
                        intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                        val uri = Uri.fromParts("package",
                            BuildConfig.APPLICATION_ID, null)
                        intent.data = uri
                        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                        startActivity(intent)
                    })
            }
        }
    }

    companion object {

        private val TAG = "LocationProvider"

        private val REQUEST_PERMISSIONS_REQUEST_CODE = 34
    }


}