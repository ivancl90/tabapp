package com.icl.tabapp.usescase.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders

class MainViewModel : ViewModel() {

    var name: String = ""

    companion object {
        fun create(mainActivity: MainActivity): MainViewModel {
            var mainViewModel = ViewModelProviders.of(mainActivity).get(MainViewModel::class.java)
            return mainViewModel
        }
    }

    /*Este metodo se llama al crear el view model*/
    init {
        name = ""
    }
}